 	

1. Quantas comparações o algoritmo faz no melhor caso? E no pior caso? 

No melhor caso do bubble sort, nenhuma comparação é feita, isto porque o vetor já estaria ordenado da maneira correta. Já no pior caso do bubble sort o os menores elementos se encontram no final do vetor, assim serão necessárias muitas iterações para trazê-los para a posição correta.


2. Quantas trocas o algoritmo faz no melhor caso? E no pior caso? 

O selection sort é um dos algoritmos que apresenta uma das menores quantidades de movimentos entre os elementos, entretanto, o número de comparações é o mesmo para o melhor caso e pior caso, sendo que seu custo é n².


3. Qual a complexidade do algoritmo no melhor caso? E no pior caso? 

No algoritmo insertion sort, o melhor caso ocorre quando o array já está ordenado, pois ele aplica várias vezes a inserção ordenada para ordenar uma sequência, assim, no melhor caso ela tem o custo O (1).
 Já no pior caso, é utilizado um array ordenado em ordem reversa, assim toda iteração na tentativa da inserção ordenada, o algoritmo deve percorrer o array todo, trocando todos os elementos até achar a posição correta.